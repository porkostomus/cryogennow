{:title "CryogenNow!"
 :layout :post
 :klipse true
 :tags  ["Cryogen"]}

### Created with Clojure!

Edit the files in `/resources/templates/md` to create your posts. Take note of the date syntax and "front-matter" map at the top.

Here is a self-hosted Clojurescript snippet:

```klipse-cljs
(ns my.site
  (:require [cljs.test :refer-macros [deftest is run-tests]]))
  
(defn my-fn [stuff]
  (str "Things and " stuff))
  
(deftest my-fn-test
  (is (= "Things and junk"
         (my-fn "junk"))))
         
(run-tests)
```

Here is a Reagent component:

```klipse-cljs
(require '[reagent.core :as r])
```

```klipse-reagent
(def bmi-data (r/atom {:height 180 :weight 80}))

(defn calc-bmi []
  (let [{:keys [height weight bmi] :as data} @bmi-data
        h (/ height 100)]
    (if (nil? bmi)
      (assoc data :bmi (/ weight (* h h)))
      (assoc data :weight (* bmi h h)))))

(defn slider [param value min max]
  [:input {:type "range" :value value :min min :max max
           :style {:width "100%"}
           :on-change (fn [e]
                        (swap! bmi-data assoc param (.-target.value e))
                        (when (not= param :bmi)
                          (swap! bmi-data assoc :bmi nil)))}])

(defn bmi-component []
  (let [{:keys [weight height bmi]} (calc-bmi)
        [color diagnose] (cond
                           (< bmi 18.5) ["orange" "underweight"]
                           (< bmi 25) ["inherit" "normal"]
                           (< bmi 30) ["orange" "overweight"]
                           :else ["red" "obese"])]
    [:div
     [:h3 "BMI calculator"]
     [:div
      "Height: " (int height) "cm"
      [slider :height height 100 220]]
     [:div
      "Weight: " (int weight) "kg"
      [slider :weight weight 30 150]]
     [:div
      "BMI: " (int bmi) " "
      [:span {:style {:color color}} diagnose]
      [slider :bmi bmi 10 50]]]))
```

## Extras

Enable *discus* in `config.edn` to add support for comments. 